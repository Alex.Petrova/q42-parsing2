package itis.parsing2;

import java.io.IOException;

interface FactoryParsingService {

    Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException, IOException;

}
