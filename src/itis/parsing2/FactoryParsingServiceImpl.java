package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException, IOException {
        List<File> fileList = new ArrayList<>();
       List<Path> fileListP = Files.walk(Paths.get(factoryDataDirectoryPath))
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        for (int i = 0; i < fileListP.size(); i++) {
            fileList.add(new File(Paths.get(String.valueOf(fileListP.get(i))).toUri()));
        }

        Class<Factory> factoryClass = Factory.class;
        List<Field> fieldList = new ArrayList<>(Arrays.asList(factoryClass.getDeclaredFields()));
        List<String> fieldListString = new ArrayList<>();
        for (Field field : fieldList) {
            fieldListString.add(field.getName());
        }

        Map<String, String> mapValue = new HashMap<>();

        for (File file : fileList) {

            try {
                Scanner scanner = new Scanner(file);

                //Создаем мапу свойств объекта
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    if (!line.equals("---")) {
                        String[] parts = line.split(":");
                        String key = parts[0].replaceAll("\"", "").trim();
                        String value = parts[1].replaceAll("\"", "").trim();
                        if (fieldListString.contains(key)) {
                            mapValue.put(key, value);
                        }
                    }
                }
                try {
                    return createFactory(mapValue);
                } catch (FactoryParsingException e) {
                    System.out.println(e.getMessage() + " *** " + e.getValidationErrors());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                System.out.println("Нет файла: " + file.toString());
            }
        }
        return null;
    }


    private Factory createFactory (Map<String, String> mapValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Class<Factory> factoryClass = Factory.class;
        Constructor<Factory> constructor = factoryClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Factory factory = constructor.newInstance();

        Field[] fields = factoryClass.getDeclaredFields();

        List<FactoryParsingException.FactoryValidationError> errors = new ArrayList<>();

        for (Field field: fields) {

            field.setAccessible(true);

            NotBlank notBlank = field.getAnnotation(NotBlank.class);
            Concatenate concatenate = field.getAnnotation(Concatenate.class);

            String fieldValueAnswer = "";

            boolean currentErrors = false;
            boolean concatFlag = false;

            if(concatenate != null){
                String[] fieldListAnnot = concatenate.fieldNames();
                String delimiter = concatenate.delimiter();
                for (int i = 0; i < fieldListAnnot.length; i++) {
                    if(mapValue.get(fieldListAnnot[i]) == null){
                        concatFlag = true;
                    }else {
                        fieldValueAnswer = fieldValueAnswer + mapValue.get(fieldListAnnot[i]) + delimiter;
                    }
                }
            }else{
                fieldValueAnswer = mapValue.get(field.getName());
            }

            if (notBlank != null && fieldValueAnswer.equals("")) {
                errors.add(
                        new FactoryParsingException.FactoryValidationError(field.getName(), "Поле null")
                );
                currentErrors = true;
            }
            if (concatenate != null && concatFlag) {
                errors.add(new FactoryParsingException.FactoryValidationError(field.getName(), "не найдена одна из составляющих")
                );
                currentErrors = true;
            }
            if (!currentErrors) {
                Class typeField = field.getType();
                field.set(factory, castValueToClass(fieldValueAnswer, typeField));
            }
        }
        if (errors.size() > 0) {
            throw new FactoryParsingException(" хрень какая - то", errors);
        } else {
            return factory;
        }
    }

    //Приводит строковое значение к нужному классу
    private Object castValueToClass (String fieldValueAnswer, Class c) {
        if (fieldValueAnswer == null || fieldValueAnswer.equals("null")) {
            return null;
        } else if (c == long.class) {
            return Long.parseLong(fieldValueAnswer);
        }else if(c == List.class){
            String trim = fieldValueAnswer.replaceAll("]", "").trim();
            String[] split = trim.split(",");
            List<String> answer = new ArrayList<>();
            for (int i = 0; i < split.length ; i++) {
                answer.add(split[i]);
            }
            return answer;
        }
        return fieldValueAnswer;
    }
}
